import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBQU856F2LzPlk353QcHA-s88a3W0yiZF0",
  authDomain: "gkeep-e75d6.firebaseapp.com",
  projectId: "gkeep-e75d6",
  storageBucket: "gkeep-e75d6.appspot.com",
  messagingSenderId: "49828780292",
  appId: "1:49828780292:web:dc3ef52855b39fc45dd851",
  // measurementId: "${config.measurementId}",
};

export const app = initializeApp(firebaseConfig);
