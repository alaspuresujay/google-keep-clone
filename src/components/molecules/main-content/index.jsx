import { Container, Divider, Grid } from "@material-ui/core";
import React from "react";
import CustomCard from "../../atoms/custom-card";
import useStyles from "./style";
import CreateNote from "../create-note";

const MainContent = ({ notes, setNotes, createNote, deleteNote }) => {
  const classes = useStyles();

  return (
    <Container className={classes.container}>
      <CreateNote createNote={createNote} setNotes={setNotes} />
      <br />
      <Divider />
      <br />

      <Grid container spacing={2}>
        {notes.map((note) => (
          <Grid key={note.id} item xs={12} sm={6} md={4} lg={3}>
            <CustomCard
              deleteNote={deleteNote}
              setNotes={setNotes}
              id={note.id}
              title={note.title}
              content={note.content}
              createdAt={note.createdAt}
            />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default MainContent;
